package com.idea.mq.service;

import java.util.List;

/**
 * Created by felayman on 15/9/23.
 */
public interface MQService {


    /**
     * 发送消息
     * @param topic
     * @param message
     */
     void  sendMsg(String topic,String message);


    /**
     * 批量发送消息
     * @param topic
     * @param messages
     */
    void batchendMsg(String topic,List<String>messages);


}
