package com.idea.mq.service.client;

import com.alibaba.rocketmq.client.exception.MQClientException;
import com.alibaba.rocketmq.client.producer.DefaultMQProducer;
import com.alibaba.rocketmq.client.producer.SendResult;
import com.alibaba.rocketmq.common.message.Message;
import com.idea.mq.service.util.Logit;
import com.idea.mq.service.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by felayman on 15/9/23.
 */
@Component
public class ProducerClient  {

    @Autowired
    private DefaultMQProducer defaultMQProducer;

    public void init(){
        try {
            defaultMQProducer.start();
        } catch (MQClientException e) {
            Logit.errorLog(e.toString(),new Throwable(e));
        }
    }

    public void shutdown(){
        defaultMQProducer.shutdown();
    }

    public void sendMsg(String topic,String message){
        try{
            if (!StrUtil.hasBlank(topic,message)){
                SendResult result  = defaultMQProducer.send(new Message(topic,message.getBytes()));
                Logit.messageLog("id:" + result.getMsgId() + ",result:" + result.getSendStatus());
            }
        }catch(Exception e){
           Logit.errorLog(e.toString(),new Throwable(e));
        }

    }

    public void sendMsg(String topic,String tag,String message){
        try{
            if (!StrUtil.hasBlank(topic,tag,message)){
                defaultMQProducer.send(new Message(topic,tag,message.getBytes()));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void sendMsg(String topic,String tag,String key,String message){
        try{
            if (!StrUtil.hasBlank(topic,tag,key,message)){
                defaultMQProducer.send(new Message(topic,tag,key,message.getBytes()));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

}
