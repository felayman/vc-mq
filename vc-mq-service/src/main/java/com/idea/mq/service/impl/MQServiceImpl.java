package com.idea.mq.service.impl;

import com.idea.mq.service.MQService;
import com.idea.mq.service.client.ProducerClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by felayman on 15/9/23.
 */
@Service("mqService")
public class MQServiceImpl implements MQService {


    @Autowired
    private ProducerClient producerClient;

    public void sendMsg(String topic, String message) {
        producerClient.sendMsg(topic,message);
    }

    public void batchendMsg(String topic, List<String> messages) {
        for (String message : messages){
            producerClient.sendMsg(topic,message);
        }
    }


}
