package com.idea.mq.service;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ProducerLauncher {

    public static void main(String [] args){
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-mq.xml");
        MQService mqService = (MQService) context.getBean("mqService");
        mqService.sendMsg("topic-a","hello");
    }

}
