package com.idea.mq.service.test;

import com.alibaba.rocketmq.client.producer.DefaultMQProducer;
import com.alibaba.rocketmq.client.producer.SendResult;
import com.alibaba.rocketmq.common.message.Message;

/**
 * Created by felayman on 15/9/17.
 */
public class Producer {

    public void produce(){
        DefaultMQProducer producer = new DefaultMQProducer("produce-a");
        producer.setNamesrvAddr("127.0.0.1:9876");
        try{
            producer.start();

            //String topic, String tags, String keys, byte[] body
            Message message = new Message("topic-a","push","1","test".getBytes());
            SendResult result = producer.send(message);
            System.out.println("id:" + result.getMsgId() + ",result:" + result.getSendStatus());

            message = new Message("topic-a","push","2","test".getBytes());
            result = producer.send(message);
            System.out.println("id:" + result.getMsgId() + ",result:" + result.getSendStatus());

            message = new Message("topic-a","push","3","test".getBytes());
            result = producer.send(message);
            System.out.println(("id:"+result.getMsgId()+",result:"+result.getSendStatus()));


        }catch(Exception e){
            e.printStackTrace();
        }finally {
            producer.shutdown();
        }
    }

    public static void main(String [] args){

        new Producer().produce();

    }

}
