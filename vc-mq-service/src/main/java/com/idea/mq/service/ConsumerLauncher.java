package com.idea.mq.service;

import com.idea.mq.service.client.ConsumerClient;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ConsumerLauncher {

    public static void main(String [] args) throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-mq.xml");
        ConsumerClient eventClient = (ConsumerClient) context.getBean("consumerClient");
        eventClient.start();
        Thread.currentThread().join();
    }

}
