package com.idea.mq.service.util;

import org.apache.log4j.Logger;

/*****************************************************
 * log 日志操作
 * @author felayman
 * @since 2015-8-4
 *****************************************************/
public class Logit {

	private static final Logger DEBUG_LOG = Logger.getLogger("vcg-debug");

	private static final Logger WARN_LOG = Logger.getLogger("vcg-warn");

	private static final Logger MESSAGE_LOG = Logger.getLogger("vcg-message");

	private static final Logger ACCESS_LOG = Logger.getLogger("vcg-access");

	private static final Logger ERROR_LOG = Logger.getLogger("sys-error");

	/**
	 * 用户访问日志
	 * @param log
	 */
	public static void accessLog(String log) {
		ACCESS_LOG.info(log);
	}

	/**
	 * 用户访问协议
	 * @param log
	 */
	public static void messageLog(String log) {
		StackTraceElement[] stacks = new Throwable().getStackTrace();
		StringBuffer sb = new StringBuffer(stacks[1].getClassName());
		sb.append(".");
		sb.append(stacks[1].getMethodName());
		sb.append("---");
		sb.append(log);
		MESSAGE_LOG.info(sb.toString());
	}

	/**
	 * 用户调试日志
	 * @param log
	 */
	public static void debugLog(String log) {
		StackTraceElement[] stacks = new Throwable().getStackTrace();
		StringBuffer sb = new StringBuffer(stacks[1].getClassName());
		sb.append(".");
		sb.append(stacks[1].getMethodName());
		sb.append("---");
		sb.append(log);
		debugPlog(sb.toString(), "debug");
	}

	/**
	 * 用户调试日志
	 * @param log
	 * @param loglevel
	 */
	public static void debugLog(String log, String loglevel) {
		StackTraceElement[] stacks = new Throwable().getStackTrace();
		StringBuffer sb = new StringBuffer(stacks[1].getClassName());
		sb.append(".");
		sb.append(stacks[1].getMethodName());
		sb.append("---");
		sb.append(log);
		debugPlog(sb.toString(), loglevel);
	}

	private static void debugPlog(String log, String loglevel) {
		if ("debug".equalsIgnoreCase(loglevel)) {
			DEBUG_LOG.debug(log);
		} else if ("info".equalsIgnoreCase(loglevel))  {
			DEBUG_LOG.info(log);
		} else if ("warn".equalsIgnoreCase(loglevel)) {
			DEBUG_LOG.warn(log);
		} else if ("error".equalsIgnoreCase(loglevel)) {
			DEBUG_LOG.error(log);
		} else {
			DEBUG_LOG.debug(log);
		}
	}

	/**
	 * 用户告警日志
	 * @param log
	 */
	public static void warnLog(String log) {
		StackTraceElement[] stacks = new Throwable().getStackTrace();
		StringBuffer sb = new StringBuffer(stacks[1].getClassName());
		sb.append(".");
		sb.append(stacks[1].getMethodName());
		sb.append("---");
		sb.append(log);
		WARN_LOG.error(sb.toString());
	}

	/**
	 * 错误日志
	 * @param log
	 */
	public static void errorLog(String log) {

		StackTraceElement[] stacks = new Throwable().getStackTrace();
		StringBuffer sb = new StringBuffer(stacks[1].getClassName());
		sb.append(".");
		sb.append(stacks[1].getMethodName());
		sb.append("---");
		sb.append(log);

		errorPLog(sb.toString(), null);
	}

	public static void errorLog(String log, Throwable throwable) {

		StackTraceElement[] stacks = new Throwable().getStackTrace();
		StringBuffer sb = new StringBuffer(stacks[1].getClassName());
		sb.append(".");
		sb.append(stacks[1].getMethodName());
		sb.append("---");
		sb.append(log);

		errorPLog(sb.toString(), throwable);
	}

	/**
	 * 错误日志
	 * @param log
	 * @param throwable
	 */
	private static void errorPLog(String log, Throwable throwable) {
		if (throwable == null) {
			ERROR_LOG.error(log);
		} else {
			ERROR_LOG.error(log, throwable);
		}
	}

	/**
	 * 自定义日志名称
	 * @param name
	 * @param log
	 */
	public static void log(String name, String log) {
		StackTraceElement[] stacks = new Throwable().getStackTrace();
		StringBuffer sb = new StringBuffer(stacks[1].getClassName());
		sb.append(".");
		sb.append(stacks[1].getMethodName());
		sb.append("---");
		sb.append(log);

		logByName(name, sb.toString(), "debug", null);
	}

	/**
	 * 自定义日志名称
	 * @param name
	 * @param log
	 * @param loglevel
	 */
	public static void log(String name, String log, String loglevel) {
		StackTraceElement[] stacks = new Throwable().getStackTrace();
		StringBuffer sb = new StringBuffer(stacks[1].getClassName());
		sb.append(".");
		sb.append(stacks[1].getMethodName());
		sb.append("---");
		sb.append(log);

		logByName(name, sb.toString(), loglevel, null);
	}

	public static void log(String name, String log, String loglevel, Throwable throwable) {
		StackTraceElement[] stacks = new Throwable().getStackTrace();
		StringBuffer sb = new StringBuffer(stacks[1].getClassName());
		sb.append(".");
		sb.append(stacks[1].getMethodName());
		sb.append("---");
		sb.append(log);

		logByName(name, sb.toString(), loglevel, throwable);
	}

	/**
	 *
	 * @param name
	 * @param log
	 * @param loglevel
	 * @param throwable
	 */
	private static void logByName(String name, String log, String loglevel, Throwable throwable) {
		if (name == null) {
			return;
		}

		StackTraceElement[] stacks = new Throwable().getStackTrace();
		StringBuffer sb = new StringBuffer(stacks[1].getClassName());
		sb.append(".");
		sb.append(stacks[1].getMethodName());
		sb.append("---");
		sb.append(log);

		Logger logger = Logger.getLogger(name);
		try {

			if (throwable == null) {
				if ("debug".equalsIgnoreCase(loglevel)) {
					logger.debug(log);
				} else if ("info".equalsIgnoreCase(loglevel))  {
					logger.info(log);
				} else if ("warn".equalsIgnoreCase(loglevel)) {
					logger.warn(log);
				} else if ("error".equalsIgnoreCase(loglevel)) {
					logger.error(log);
				} else {
					logger.debug(log);
				}
			} else {
				if ("debug".equalsIgnoreCase(loglevel)) {
					logger.debug(log, throwable);
				} else if ("info".equalsIgnoreCase(loglevel))  {
					logger.info(log, throwable);
				} else if ("warn".equalsIgnoreCase(loglevel)) {
					logger.warn(log, throwable);
				} else if ("error".equalsIgnoreCase(loglevel)) {
					logger.error(log, throwable);
				} else {
					logger.debug(log, throwable);
				}

			}
		} catch (Exception ex) {
			// 不做处理
		}
	}

}
