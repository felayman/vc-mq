package com.idea.mq.service.client;

import com.alibaba.rocketmq.client.consumer.DefaultMQPushConsumer;
import com.alibaba.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import com.alibaba.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import com.alibaba.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import com.alibaba.rocketmq.common.message.Message;
import com.alibaba.rocketmq.common.message.MessageExt;
import com.idea.mq.service.bean.MessageBean;
import com.idea.mq.service.util.Logit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by felayman on 15/9/24.
 */
@Component
public class ConsumerClient {

    @Autowired
    private DefaultMQPushConsumer consumer;
    @Autowired
    private MessageBean messageBean;

    public void start(){
        try{
            String topic = messageBean.getTopic();
            consumer.subscribe(topic,null);
            consumer.registerMessageListener(new MessageListenerConcurrently() {
                        public ConsumeConcurrentlyStatus consumeMessage( List<MessageExt> list,ConsumeConcurrentlyContext Context) {
                            Message message = list.get(0);
                            Logit.messageLog("the message is :"+message);
                            return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
                        }
                    }
            );
            consumer.start();
        }catch(Exception e){
           Logit.errorLog(e.toString(),new Throwable(e));
        }
    }

}
